package com.example.recycleview.activity;

import com.example.recycleview.domain.Interactor;
import com.example.recycleview.domain.InteractorContract;

import io.reactivex.disposables.CompositeDisposable;

public class Presenter implements PresenterContract.Presenter {
    private PresenterContract.View view;
    private CompositeDisposable disposable;
    private InteractorContract interactor;

    public Presenter() {
        disposable = new CompositeDisposable();
        interactor = new Interactor();
    }

    public void init(){
        disposable.add(interactor.getList()
                .subscribe(v -> {
                    view.initDisplay(v);
                }));
    }



    @Override
    public void starView(PresenterContract.View view) {
        this.view = view;
    }

    @Override
    public void stopView() {
        view=null;
        disposable.clear();

    }
}

