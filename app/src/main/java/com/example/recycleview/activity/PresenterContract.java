package com.example.recycleview.activity;

import com.example.recycleview.BasePresenter;
import com.example.recycleview.model.Model;

import java.util.List;

public interface PresenterContract {
    interface View {
        void initDisplay(List<Model> list);
    }

    interface Presenter extends BasePresenter<View> {
        void init();
    }
}
