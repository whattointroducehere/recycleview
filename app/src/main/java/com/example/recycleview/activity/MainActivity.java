package com.example.recycleview.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.example.recycleview.Adapter;
import com.example.recycleview.R;
import com.example.recycleview.RollingLayoutManager;
import com.example.recycleview.model.Model;

import java.util.List;

public class MainActivity extends AppCompatActivity implements PresenterContract.View {
    private PresenterContract.Presenter presenter;
    private Adapter adapter;
    private RecyclerView rvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvMain = this.findViewById(R.id.rv_main);
        presenter = new Presenter();
        presenter.init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.starView(this);
    }

    @Override
    protected void onPause() {
        presenter.stopView();
        super.onPause();
    }

    @Override
    public void initDisplay(List<Model> list) {
        RecyclerView.LayoutManager layoutManager = new RollingLayoutManager(this);
        rvMain.setLayoutManager(layoutManager);
        adapter = new Adapter(list,presenter);
        rvMain.setAdapter(adapter);
    }
}

