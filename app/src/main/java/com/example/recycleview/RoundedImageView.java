package com.example.recycleview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.graphics.Path;


public class RoundedImageView extends AppCompatImageView {

    private float topLeft,topRight,bottomLeft,bottomRight;
    private Path path;

    public RoundedImageView(Context context) {
        this(context,null);
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        final TypedArray a = context.obtainStyledAttributes(
                attrs, R.styleable.RoundedImageView, 0, defStyle);
        topLeft = a.getDimension(R.styleable.RoundedImageView_topLeft, 0);
        topRight = a.getDimension(R.styleable.RoundedImageView_topRight, 0);
        bottomLeft = a.getDimension(R.styleable.RoundedImageView_bottomLeft, 0);
        bottomRight = a.getDimension(R.styleable.RoundedImageView_bottomRight, 0);
        a.recycle();
        init();

    }
    private void init() {
        path = new Path();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        path = getPath(w, h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.clipPath(path);
        super.onDraw(canvas);
    }

    private Path getPath(int w, int h) {

        final Path path = new Path();
        final float[] radii = new float[8];

        radii[0] = topLeft;
        radii[1] = topLeft;
        radii[2] = topRight;
        radii[3] = topRight;
        radii[4] = bottomRight;
        radii[5] = bottomRight;
        radii[6] = bottomLeft;
        radii[7] = bottomLeft;

        path.addRoundRect(new RectF(0, 0, w, h),
                radii, Path.Direction.CW);
        return path;
    }
}
