package com.example.recycleview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.recycleview.activity.PresenterContract;
import com.example.recycleview.model.Model;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<ViewHolderMain> {
    private List<Model> list;
    private PresenterContract.Presenter listener;

    public Adapter(List<Model> list,PresenterContract.Presenter listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolderMain onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        return new ViewHolderMain(view,viewGroup.getContext(),listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderMain viewHolderMain, int i) {
        viewHolderMain.bind(list.get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}