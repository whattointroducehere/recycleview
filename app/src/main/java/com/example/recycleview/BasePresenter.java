package com.example.recycleview;

public interface BasePresenter<T> {

    void starView(T view);

    void stopView();
}
