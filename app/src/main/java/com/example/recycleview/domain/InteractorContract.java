package com.example.recycleview.domain;

import com.example.recycleview.model.Model;

import java.util.List;

import io.reactivex.Single;

public interface InteractorContract {
    Single<List<Model>> getList();
}
