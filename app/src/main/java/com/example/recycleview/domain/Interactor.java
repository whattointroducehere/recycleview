package com.example.recycleview.domain;

import com.example.recycleview.BaseInteractor;
import com.example.recycleview.model.Mock;
import com.example.recycleview.model.Model;

import java.util.List;

import io.reactivex.Single;

public class Interactor extends BaseInteractor implements InteractorContract {

    @Override
    public Single<List<Model>> getList() {
        return Mock.getInstance().getList()
                .compose(applySingleSchedulers());
    }
}
