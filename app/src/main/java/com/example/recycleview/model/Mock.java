package com.example.recycleview.model;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

public class Mock {
    private List<Model> list;
    private static Mock instance;

    private Mock(){
        list = new ArrayList<>();
    }

    public static synchronized Mock getInstance(){
        if(instance == null){
            instance=new Mock();
        }
        return instance;
    }

    public Single<List<Model>> getList(){
        list.add(new Model("https://storage1a.censor.net/images/b/8/6/7/b86703fa22d362f541bde6f24e30bdd1/original.jpg","GOOGLE"));
        list.add(new Model("https://storage1a.censor.net/images/b/8/6/7/b86703fa22d362f541bde6f24e30bdd1/original.jpg","GOOGLE"));
        list.add(new Model("https://storage1a.censor.net/images/b/8/6/7/b86703fa22d362f541bde6f24e30bdd1/original.jpg","GOOGLE"));
        list.add(new Model("https://storage1a.censor.net/images/b/8/6/7/b86703fa22d362f541bde6f24e30bdd1/original.jpg","GOOGLE"));
        list.add(new Model("https://storage1a.censor.net/images/b/8/6/7/b86703fa22d362f541bde6f24e30bdd1/original.jpg","GOOGLE"));
        list.add(new Model("https://storage1a.censor.net/images/b/8/6/7/b86703fa22d362f541bde6f24e30bdd1/original.jpg","GOOGLE"));
        list.add(new Model("https://storage1a.censor.net/images/b/8/6/7/b86703fa22d362f541bde6f24e30bdd1/original.jpg","GOOGLE"));
        list.add(new Model("https://storage1a.censor.net/images/b/8/6/7/b86703fa22d362f541bde6f24e30bdd1/original.jpg","GOOGLE"));
        list.add(new Model("https://storage1a.censor.net/images/b/8/6/7/b86703fa22d362f541bde6f24e30bdd1/original.jpg","GOOGLE"));
        list.add(new Model("https://storage1a.censor.net/images/b/8/6/7/b86703fa22d362f541bde6f24e30bdd1/original.jpg","GOOGLE"));


        return Single.just(list);

}
    }
