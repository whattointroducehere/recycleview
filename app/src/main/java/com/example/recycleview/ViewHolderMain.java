package com.example.recycleview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.recycleview.activity.PresenterContract;
import com.example.recycleview.model.Model;

public class ViewHolderMain extends RecyclerView.ViewHolder {

    private AppCompatTextView tv;
    private AppCompatImageView riv;
    private Context context;

    public ViewHolderMain(@NonNull View itemView, Context context, PresenterContract.Presenter listener) {
        super(itemView);
        this.context = context;
        tv = itemView.findViewById(R.id.textView);
        riv = itemView.findViewById(R.id.image);
    }

    public void bind(Model item) {
        if (item != null) {
            tv.setText(item.getName());
            RequestOptions ro = new RequestOptions()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .override(300, 300)
                    .fitCenter();
            Glide.with(itemView)
                    .load(item.getUrl())
                    .centerCrop()
                    .into(riv);

        }
    }
}
